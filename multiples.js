function multiples(index){

    /*
        se iguala la variable counter a un entero
        si este entero es multiplo de 3 la variable counter se iguala a un string Foo
        si este entero es multiplo de 5 la variable counter se iguala a un string Bar
        si este entero es multiplo de 3 y 5 la variable counter se iguala a un string FooBar
    */

    let counter = index;
    let m3 = index % 3;
    let m5 = index % 5;

    /*  
        Solo se permite el uso de una condición if y no se puede usar else o un operador ternario.
        Mi solución fue usar cilos while o tambien for para pasar un entero,
        si el entero cae en alguno de los cilclos while o for el loop se detiene y la función regresa la variable counter con su respectivo valor
    */
    

    while (m3 == 0) {
        counter = 'Foo'; 
        break; 
    }

    while (m5 == 0) { 
        counter = 'Bar'; 
        break; 
    }

    while (m3 == 0 && m5 == 0) { 
        counter = 'FooBar'; 
        break; 
    }


    // prueba con  for loop
    /*for(; m3 == 0;){
        counter = 'Foo'; 
        break;
    }

    for(; m5 == 0;){
        counter = 'Bar'; 
        break;
    }

    for(; m3 == 0 && m5 == 0;){
        counter = 'FooBar'; 
        break;
    }*/


    return counter;
}



function execMultiples(){
    //arreglo auxiliar para guardar la data que regrese la función multiples
    aux = [];
    
    //se hace un loop hasta 100 y se le pasa la función multiples()
    //cada que itera la funcion se agrega la nueva data al arreglo aux
    for(let i = 0; i < 100; i++){
        aux.push(multiples(i));
    }
    
    //recorremos el arreglo aux para imprimir su contenido ya formateado
    for(let j = 0; j < aux.length; j++){
        console.log(aux[j]);
    }

}


module.exports = execMultiples; 